import socket #import library socket

#menentukan ip address yang dituju dan port yang dituju
ip = '127.0.0.1'
port = 3000
addr = (ip,port)

#membuat socket
sc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#mengirim pesan ke server
#parameter : pesan yang akan dikirim, alamat server
sc.sendto("Assalamualaikum  Server " .encode(),addr)

#menangkap pesan dari server
data,addr = sc.recvfrom(1024)

#cetak pesan
print(data)

#menutup socket
sc.close()