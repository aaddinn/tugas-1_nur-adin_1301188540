import socket               # Import socket module

sc = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 12345                # Reserve a port for your service.

sc.connect((host, port))
print(sc.recv(1024))
sc.close