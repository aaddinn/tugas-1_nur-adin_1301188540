import socket #import library socket

#menentukan  alamat ip bind dari server dan port number untuk bind dari server
ip = '127.0.0.1'
port = 3000
addr = (ip,port)

#membuat socket bertipe UDP
sc = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

#bind
sc.bind(addr)

#menangkap pesan dari client
#input : size
#output : pesan dan ip address klien
data, addr = sc.recvfrom(1024)
c=data.decode()

#cetak pesan
print("Pesan : " + c)

#mengirim data kembali ke klien
data=" Pesan : Walaikumsalam Client ".encode()
sc.sendto(data,addr)

#menutup socket
sc.close()