from xmlrpc.server import SimpleXMLRPCServer
import xmlrpc.client

def file_download():
    handle = open("file_didownload.txt",'rb') #syntax untuk membuk file dengan rb yaitu untuk membaca file sebagai binary (read binary)
    return xmlrpc.client.Binary(handle.read()) #untuk mengirim data ke klien dengan format binary
    handle.close() #menutup file

server = SimpleXMLRPCServer(('localhost',8000)) #inisialisasi socket server
print ("Listening on port 8000")

server.register_function(file_download,'download') #syntax untuk meregister fungsi untuk menerima respon data dari klien

server.serve_forever() #method untuk memulai server dengan socket yang telah ditentukan diatas