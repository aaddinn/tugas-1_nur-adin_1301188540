import socket               # Import socket module

sc= socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 12345                # Reserve a port for your service.
sc.bind((host, port))        # Bind to the port

sc.listen(5)
pesan='Telah Terkoneksi'.encode()
# Now wait for client connection.
while True:
   c, addr = sc.accept()     # Establish connection with client.
   print('Menerima Koneksi Dari : ', addr)
   c.send(pesan)
   c.close()                # Close the connection